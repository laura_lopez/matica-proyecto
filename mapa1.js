let miMapa = L.map('mapid'); 

//Determinar la vista inicial
miMapa.setView([4.692896,-74.150819], 13);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
miProveedor.addTo(miMapa);


//GeoJSON: JSON que describe objetos geograficos
// puntos (point)
//lineas (linestring)
//Poligono (Polygon)
//Multiples lineas (MultilineString)
//Multiles poligonos (MultyPolygon)
//Coleccion geometrias (Geometry Collection)
//Entidad Geografica (Features): Objeto geografico = Metadata

//Se recomienda que un GeoJSON define features, no solo la geometria 

// Crear un objeto (variable)
//L representa a la bibioteca leaflet

//Para agregarlo a mi mapa 

let miGeoJson=L.geoJson(sitio);
miGeoJson.addTo(miMapa);
var greenIcon = L.icon({
    iconUrl : 'planta.png' ,
    shadowUrl: 'leaf-shadow.png',
    
    iconSize :    [38, 95],
    shadowSize:   [50, 64],
    iconAnchor:   [22, 94],
    shadowAnchor: [4, 62], 
    popupAnchor:  [-3, -76]
})



//Crear un objeto marcador
let miMarcador = L.marker([4.690453,-74.147257]);
miMarcador.addTo(miMapa);

miMarcador.bindPopup("<br>Nombre de la Granja: virtualGreenFarm<br>Nombre agricultor: Laura Alejandra Lopez Diaz<br>Nombre comun de la planta: Lenteja<br>Nombre cientifico: Lens Culinaris<br>Fecha de plantacion: 14/12/2020<br>Humedad Relativa: Mayor a 15%<br>Familia: Fabaceae<br>Genero: Lens<br>Condicion Climatica: (de 300 a 450 mm)<br>Temperatura Optica: entre 6 a 28ºC<br>PH del Suelo: 5.5 a 9<br>Piso Termico: Frio,Templado y calido<br>Ubicacion: [4.690453,-74.147257]").openPopup();
miMarcador.addTo(miMapa);

//let miMarcador2 = L.marker([4.690453,-74.147257]);
miMarcador.addTo(miMapa);

//JSON
let circle = L.circle([4.690443,-74.147250], {
    color: 'pink',
    fillColor: 'pink',
    fillOpacity: 0.5,
    radius: 400
});

circle.addTo(miMapa);
